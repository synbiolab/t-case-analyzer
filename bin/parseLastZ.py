#!/usr/bin/env python3

def parseLastZ(LastZO, outSel, outTrim, id = 00, len = 00):
	"""Parse LastZ output file to obtain the name of the reads that contain a PERV hit.
	Parse LastZ output file to obtain the name and position of the longest hit for following trimming."""
	infile = open(LastZO)
	toSelect = set()
	toTrim = []
	longestTrim = []
	
	infile.readline()
	for line in infile:
		line = line.strip()
		line = line.split("\t")
		if float(line[12][:-1]) > id and float(line[14][:-1]) > len:
			toSelect.add(line[1])
			toTrim.append((line[1], line[4], line[5]))
			
	first = True
	for hit in toTrim:
		if first:
			name = hit[0]
			st = int(hit[1])
			end = int(hit[2])
			first = False
		else:
			if hit[0] == name:
				if (end - st) < (int(hit[2]) - int(hit[1])):
					st = int(hit[1])
					end = int(hit[2])
					name = hit[0]
			else:
				longestTrim.append((name, st, end))
				name = hit[0]
				st = int(hit[1])
				end = int(hit[2])

	longestTrim.append((name, st, end))
	
	selFile = open(outSel, 'w')
	for selName in toSelect:
		selFile.write(selName + "\n")
	selFile.close()
	
	trimFile = open(outTrim, 'w')
	for trimHit in longestTrim:
		trimFile.write("%s\t%d\t%d\n" %(trimHit[0], trimHit[1], trimHit[2]))
	trimFile.close()

if __name__ == "__main__":
	import argparse
	
	parser = argparse.ArgumentParser()
	parser.add_argument("i", help="Input txt file containing the output of LastZ alignment in tabular format.", action="store")
	parser.add_argument("os", help="Output txt file containing the names of sequences that will be selected.", action="store")
	parser.add_argument("ot", help="Output txt file containing the positions of the alginment hit.", action="store")
	parser.add_argument("-id", type=int, help="% identity value treshold (filter hits by higher than -id).", action="store")
	parser.add_argument("-len", type=int, help="% length value treshold (filter hits by higher than -len).", action="store")
	args = parser.parse_args()
	
	parseLastZ(args.i, args.os, args.ot, args.id, args.len)
