#!/usr/bin/env python3

def detectPairs(DF, at5p, at3p, dist):
	"""Parses data frame from R detecting reads that conform a both-side trucated PERV"""
	import pandas
	import numpy
	from rpy2.robjects import pandas2ri
	
	pandas2ri.activate()
	dim = DF.shape
	pairs = []
	
	for i in DF.iterrows():
		ind = DF.index.get_loc(i[0])
		subDF = DF.tail(dim[0]-(ind+1))
		for j in subDF.iterrows():
			if i[1]['seqnames'] == j[1]['seqnames']:
				if (i[0] in at5p) and (j[0] in at3p):
					if (j[1]['start']-i[1]['start']) <= dist and (j[1]['start']-i[1]['start']) > 0:
						pairs.append((i[0], j[0]))
				elif (i[0] in at3p) and (j[0] in at5p):
					if (i[1]['start']-j[1]['end']) <= dist and (i[1]['start']-j[1]['end']) > 0:
						pairs.append((j[0], i[0]))
	
	return (DF.index.values, pairs)

def parseNames(infile):
	"""Parses files containing list of names"""
	file = open(infile)
	list = []
	
	for line in file:
		line = line.strip()
		list.append(line)
	
	return list

def retieveIndex(trunc5, trunc3, names, pairs):
	"""Returns the index of each read (ordered as the original data frame) to add as data frame metadata.
	1: complete
	2: 5' truncated
	3: 3' truncated
	4: both truncated
	5: half complete
	6: half 5' truncated
	7: half 3' truncated"""
	pairIndex = {}
	for p in pairs:
		if (p[0] in trunc5 and p[1] in trunc3) or (p[0] in trunc3 and p[1] in trunc5):
			pairIndex[p[0]] = 4 # both truncated
			pairIndex[p[1]] = 4
		elif (p[0] in trunc5 and p[1] not in trunc3) or (p[0] not in trunc3 and p[1] in trunc5):
			pairIndex[p[0]] = 2 # 5' truncated
			pairIndex[p[1]] = 2
		elif (p[0] in trunc3 and p[1] not in trunc5) or (p[0] not in trunc5 and p[1] in trunc3):
			pairIndex[p[0]] = 3 # 3' truncated
			pairIndex[p[1]] = 3
		else:
			pairIndex[p[0]] = 1 # complete
			pairIndex[p[1]] = 1
	
	finalIndex = []
	for n in names:
		if n in pairIndex.keys():
			finalIndex.append(pairIndex[n])
		else:
			if n in trunc5:
				finalIndex.append(6) # half 5' truncated
			elif n in trunc3:
				finalIndex.append(7) # half 3' truncated
			else:
				finalIndex.append(5) # half compete
	
	return finalIndex

def writePERVbed(DF, names, pairs, at5p, bedF):
	"""Write bed file for reconstituted PERV sequences"""
	import pandas
	from rpy2.robjects import pandas2ri
	
	pandas2ri.activate()
	bed = open(bedF, 'w')
	#bed.write('track name="PERVs" description="PERVs colored by state" type="bigBed" itemRgb="On" useScore=1\n')
	
	RGB = {"complete": "231,76,60", "5'_truncated": "136,78,160", "3'_truncated": "36,113,163", 
	"both_truncated": "46,204,113", "half_complete": "241,196,15", "half_5'_truncated": "211,84,0", 
	"half_3'_truncated": "236,64,122"}
	
	for p in pairs:
		chr = DF.at[p[0], 'seqnames']
		strand = DF.at[p[0], 'strand']
		itemRGB = RGB[DF.at[p[0], 'type']]
		if DF.at[p[0], 'end'] < DF.at[p[1], 'start']: # NO overlaping
			score = 1000
			start = DF.at[p[0], 'end']
			end = DF.at[p[1], 'start']
		else: # if there is an overlaping
			score = 500
			start = DF.at[p[0], 'start']
			end = DF.at[p[1], 'end']
		name = p[0] + "-" + p[1] + "-" + DF.at[p[0], 'type']
		thickStart = end
		thickEnd = end
		bed.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(chr, start, end, name, score, strand, thickStart, thickEnd, itemRGB))
	
	pNames = list(sum(pairs, []))
	for n in names:
		if n not in pNames:
			score = 1000
			chr = DF.at[n, 'seqnames']
			strand = DF.at[n, 'strand']
			itemRGB = RGB[DF.at[n, 'type']]
			name = n + "-" + DF.at[n, 'type']
			if n in at5p:
				start = DF.at[n, 'end']
				end = DF.at[n, 'end'] + 1
			else:
				start = DF.at[n, 'start'] - 1
				end = DF.at[n, 'start']
			thickStart = end
			thickEnd = end
			bed.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(chr, start, end, name, score, strand, thickStart, thickEnd, itemRGB))
	
