#!/usr/bin/env python3

def retrieveLastZpositions(lastFile, reads):
	"""Parses the file created by parsing the LastZ output to return it in a propper variable format."""
	lastz = open(lastFile)
	positions = {}
	
	for read, trim in zip(reads, lastz):
		trim = trim.strip()
		trim = trim.split("\t")
		positions[read[0]] = (int(trim[1]), int(trim[2]))
	
	return positions

def trimSequences(reads, pol5_positions, pol3_positions, LTRU3_positions, LTRU5_positions, LastZ_positions, fiveN, threeN, compN, trun5N, trun3N):
	"""Trims the sequences from the first LTR match to the end (pol match) or the begining (pol match) to the last LTR match."""
	trimmed = []
	complete = [] # not truncated PERV
	at5p = [] # reads located at 5' from PERV
	at3p = [] # reads located at 3' from PERV
	trunc5 = [] # 5' truncated PERV
	trunc3 = [] # 3' truncated PERV
	
	for read in reads:
		subreads = []
		lastz = True
		trunc = True
		### normal cases
		if (len(pol5_positions[read[0]]["normal"]) > 0) and (len(LTRU3_positions[read[0]]["normal"]) > 0) and (LTRU3_positions[read[0]]["normal"][0][1] < pol5_positions[read[0]]["normal"][0][0]):
			subreads.append(read[1][0:LTRU3_positions[read[0]]["normal"][0][0]]) # trim read from the start of the first U3 match to the end
			lastz = False
			trunc = False
		else:
			subreads.append("0")
		if (len(pol5_positions[read[0]]["rc"]) > 0) and (len(LTRU3_positions[read[0]]["rc"]) > 0) and (LTRU3_positions[read[0]]["rc"][-1][0] > pol5_positions[read[0]]["rc"][-1][1]):
			subreads.append(read[1][LTRU3_positions[read[0]]["rc"][-1][1]:]) # trim read from the start to the end of the last U3 match
			lastz = False
			trunc = False
		else:
			subreads.append("0")
		if (len(pol3_positions[read[0]]["normal"]) > 0) and (len(LTRU5_positions[read[0]]["normal"]) > 0) and (LTRU5_positions[read[0]]["normal"][-1][0] > pol3_positions[read[0]]["normal"][-1][1]):
			subreads.append(read[1][LTRU5_positions[read[0]]["normal"][-1][1]:]) # trim read from the start to the end of the last U5 match
			lastz = False
			trunc = False
		else:
			subreads.append("0")
		if (len(pol3_positions[read[0]]["rc"]) > 0) and (len(LTRU5_positions[read[0]]["rc"]) > 0) and (LTRU5_positions[read[0]]["rc"][0][1] < pol3_positions[read[0]]["rc"][0][0]):
			subreads.append(read[1][0:LTRU5_positions[read[0]]["rc"][0][0]]) # trim read from the start of the first U5 match to the end
			lastz = False
			trunc = False
		else:
			subreads.append("0")
		### other cases
		### if the expected LTR piece does not match but the other yes, we won't cut all the PERV (unless it is not complete) but we will cut most of it
		if trunc:
			if (len(pol5_positions[read[0]]["normal"]) > 0) and (len(LTRU5_positions[read[0]]["normal"]) > 0) and (LTRU5_positions[read[0]]["normal"][0][1] < pol5_positions[read[0]]["normal"][0][0]):
				subreads.append(read[1][0:LTRU5_positions[read[0]]["normal"][0][0]]) # trim read from the start of the first U5 match to the end
				lastz = False
			else:
				subreads.append("0")
			if (len(pol5_positions[read[0]]["rc"]) > 0) and (len(LTRU5_positions[read[0]]["rc"]) > 0) and (LTRU5_positions[read[0]]["rc"][-1][0] > pol5_positions[read[0]]["rc"][-1][1]):
				subreads.append(read[1][LTRU5_positions[read[0]]["rc"][-1][1]:]) # trim read from the start to the end of the last U5 match
				lastz = False
			else:
				subreads.append("0")
			if (len(pol3_positions[read[0]]["normal"]) > 0) and (len(LTRU3_positions[read[0]]["normal"]) > 0) and (LTRU3_positions[read[0]]["normal"][-1][0] > pol3_positions[read[0]]["normal"][-1][1]):
				subreads.append(read[1][LTRU3_positions[read[0]]["normal"][-1][1]:]) # trim read from the start to the end of the last U3 match
				lastz = False
			else:
				subreads.append("0")
			if (len(pol3_positions[read[0]]["rc"]) > 0) and (len(LTRU3_positions[read[0]]["rc"]) > 0) and (LTRU3_positions[read[0]]["rc"][0][1] < pol3_positions[read[0]]["rc"][0][0]):
				subreads.append(read[1][0:LTRU3_positions[read[0]]["rc"][0][0]]) # trim read from the start of the first U3 match to the end
				lastz = False
			else:
				subreads.append("0")
		### if any of the HMM matches, we trim by the LastZ alignment positions
		if lastz:
			trimSeq = read[1][0:LastZ_positions[read[0]][0]] + read[1][LastZ_positions[read[0]][1]:]
			if LastZ_positions[read[0]][0] > len(trimSeq)-LastZ_positions[read[0]][0]: # if left part of the original read is longer
				at5p.append(read[0])
				trunc5.append(read[0])
			else: # if right part of the original read is longer
				at3p.append(read[0])
				trunc3.append(read[0])
		else:
			trimSeq = max(subreads, key=len)
			ind = subreads.index(trimSeq)
			if ind == 0 or ind == 1:
				complete.append(read[0])
				at5p.append(read[0])
			elif ind == 2 or ind == 3:
				complete.append(read[0])
				at3p.append(read[0])
			elif ind == 4 or ind == 5:
				trunc5.append(read[0])
				at5p.append(read[0])
			elif ind == 6 or ind == 7:
				trunc3.append(read[0])
				at3p.append(read[0])
		trimmed.append((read[0], trimSeq))
	
	writeNames(fiveN, at5p)
	writeNames(threeN, at3p)
	writeNames(compN, complete)
	writeNames(trun5N, trunc5)
	writeNames(trun3N, trunc3)
	
	return trimmed

def writeNames(file, names):
	"""Writes a text file with the names of the reads"""
	out = open(file, 'w')
	
	for n in names:
		if n.startswith(">"):
			n = n.split(">")[1]
		out.write("{}\n".format(n))

def writeTrimmed(newReads, outFile):
	"""Writes a file with the trimmed reads."""
	file = open(outFile, 'w')
	
	for read in newReads:
		file.write('{}\n{}\n'.format(read[0], read[1]))

def applyHMMtoReads(reads, k, s, model, probCutoff):
	"""Applyes forward algorithm to calculate the log probability of each sequence fragment.
	Being k k-mers and s stepping.
	If log probability <= probCutoff, it's considered as a match.
	Returns a dictionary with the name of each read and the initial and end positions of the sequence of each match."""
	from Bio.Seq import Seq
	
	results = {}
	probs = []
	
	for read in reads:
		seq = Seq(read[1]).ungap("U").ungap("R").ungap("Y").ungap("S").ungap("W").ungap("K").ungap("M").ungap("B").ungap("D").ungap("H").ungap("V").ungap("N").ungap(".").ungap("-")
		
		results[read[0]] = {"normal":[], "rc":[]}
		
		if k == 0: # we check the complete sequence instead of using k-mers and stepping
			if sequenceProbability(seq, model) >= probCutoff:
				probs.append(sequenceProbability(seq, model))
			if sequenceProbability(seq.reverse_complement(), model) >= probCutoff:
				probs.append(sequenceProbability(seq.reverse_complement(), model))
		else:
			init = 0
			end = k - 1
			while end <= len(seq):
				if sequenceProbability(seq[init:end], model) >= probCutoff:
					results[read[0]]["normal"].append((init, end))
					probs.append(sequenceProbability(seq[init:end], model))
				if sequenceProbability(seq.reverse_complement()[init:end], model) >= probCutoff:
					results[read[0]]["rc"].append((init, end))
					probs.append(sequenceProbability(seq.reverse_complement()[init:end], model))
				init = init + s
				end = end + s
	
	return (results, probs)

def parseReadsFile(readFile):
	"""Parses the fasta file containing reads and returns a list with tupels containint the names of the reads and the sequence."""
	reads = open(readFile)
	
	first = True
	parsed = []
	for line in reads:
		line = line.strip()
		if first:
			seq = ""
			name = line
			first = False
		else:
			if line.startswith(">"):
				parsed.append((name, seq))
				name = line
				seq = ""
			else:
				seq = seq + line.upper()
	parsed.append((name, seq))
	
	return parsed

def sequenceProbability(sequence, model):
	"""Returns the logarithmic probability of a sequence given a HMM."""
	return model.log_probability(sequence)

def loadHMMProfile(inFile):
	"""Create a HMM object from the HMM in a JSON file."""
	import pomegranate as pg
	
	model = pg.HiddenMarkovModel.from_json(inFile)
	
	return model

def runRpol():
	"""Runs all this script suitable for further R applications for pol gene."""
	import multiprocessing as mp
	
	testingSets = ["TestingSets_pol/5pol_last50_Sequence-Set_Basic_Testing1_Mutation_Negative.fasta", "TestingSets_pol/5pol_last50_Sequence-Set_Basic_Testing2_Mutation_Negative.fasta", "TestingSets_pol/5pol_last50_Sequence-Set_Mutation_Testing1_Negative.fasta", "TestingSets_pol/5pol_last50_Sequence-Set_Mutation_Testing2_Negative.fasta", "TestingSets_pol/5pol_last100_Sequence-Set_Basic_Testing1_Mutation_Negative.fasta", "TestingSets_pol/5pol_last100_Sequence-Set_Basic_Testing2_Mutation_Negative.fasta", "TestingSets_pol/5pol_last100_Sequence-Set_Mutation_Testing1_Negative.fasta", "TestingSets_pol/5pol_last100_Sequence-Set_Mutation_Testing2_Negative.fasta", "TestingSets_pol/5pol_Sequence-Set_Basic_Testing1_Mutation-Negative.fasta", "TestingSets_pol/5pol_Sequence-Set_Basic_Testing2_Mutation-Negative.fasta", "TestingSets_pol/5pol_Sequence-Set_Mutation_Testing1_Negative.fasta", "TestingSets_pol/5pol_Sequence-Set_Mutation_Testing2_Negative.fasta", "TestingSets_pol/3pol_first50_Sequence-Set_Basic_Testing1_Mutation_Negative.fasta", "TestingSets_pol/3pol_first50_Sequence-Set_Basic_Testing2_Mutation_Negative.fasta", "TestingSets_pol/3pol_first50_Sequence-Set_Mutation_Testing1_Negative.fasta", "TestingSets_pol/3pol_first50_Sequence-Set_Mutation_Testing2_Negative.fasta", "TestingSets_pol/3pol_first100_Sequence-Set_Basic_Testing1_Mutation_Negative.fasta", "TestingSets_pol/3pol_first100_Sequence-Set_Basic_Testing2_Mutation_Negative.fasta", "TestingSets_pol/3pol_first100_Sequence-Set_Mutation_Testing1_Negative.fasta", "TestingSets_pol/3pol_first100_Sequence-Set_Mutation_Testing2_Negative.fasta", "TestingSets_pol/3pol_Sequence-Set_Basic_Testing1_Mutation-Negative.fasta", "TestingSets_pol/3pol_Sequence-Set_Basic_Testing2_Mutation-Negative.fasta", "TestingSets_pol/3pol_Sequence-Set_Mutation_Testing1_Negative.fasta", "TestingSets_pol/3pol_Sequence-Set_Mutation_Testing2_Negative.fasta"]
	
	testingReads = []
	for set in testingSets:
		testingReads.append(parseReadsFile(set))
	
	jsons = ["JSONs_pol/5pol_last50_Sequence-Set_Basic_Training1.json", "JSONs_pol/5pol_last50_Sequence-Set_Basic_Training2.json", "JSONs_pol/5pol_last50_Sequence-Set_Mutation_Training1.json", "JSONs_pol/5pol_last50_Sequence-Set_Mutation_Training2.json", "JSONs_pol/5pol_last100_Sequence-Set_Basic_Training1.json", "JSONs_pol/5pol_last100_Sequence-Set_Basic_Training2.json", "JSONs_pol/5pol_last100_Sequence-Set_Mutation_Training1.json", "JSONs_pol/5pol_last100_Sequence-Set_Mutation_Training2.json", "JSONs_pol/5pol_Sequence-Set_Basic_Training1.json", "JSONs_pol/5pol_Sequence-Set_Basic_Training2.json", "JSONs_pol/5pol_Sequence-Set_Mutation_Training1.json", "JSONs_pol/5pol_Sequence-Set_Mutation_Training2.json", "JSONs_pol/3pol_first50_Sequence-Set_Basic_Training1.json", "JSONs_pol/3pol_first50_Sequence-Set_Basic_Training2.json", "JSONs_pol/3pol_first50_Sequence-Set_Mutation_Training1.json", "JSONs_pol/3pol_first50_Sequence-Set_Mutation_Training2.json", "JSONs_pol/3pol_first100_Sequence-Set_Basic_Training1.json", "JSONs_pol/3pol_first100_Sequence-Set_Basic_Training2.json", "JSONs_pol/3pol_first100_Sequence-Set_Mutation_Training1.json", "JSONs_pol/3pol_first100_Sequence-Set_Mutation_Training2.json", "JSONs_pol/3pol_Sequence-Set_Basic_Training1.json", "JSONs_pol/3pol_Sequence-Set_Basic_Training2.json", "JSONs_pol/3pol_Sequence-Set_Mutation_Training1.json", "JSONs_pol/3pol_Sequence-Set_Mutation_Training2.json"]
	
	models = []
	for m in jsons:
		models.append(loadHMMProfile(m))
	
	processes = mp.Pool(processes = 16)
	results = [processes.apply_async(applyHMMtoReads, args=(r, 0, 0, m, -10000000)) for r, m in zip(testingReads, models)]
	VectorList = [p.get() for p in results]
	
	return VectorList

def runRLTR():
	"""Runs all this script suitable for further R applications for LTR U3 and U5 regions."""
	import multiprocessing as mp
	
	testingSets = ["TestingSets/LTR_Sequence-Set_Testing_Mutation_Negatives.fasta", "TestingSets/LTR_Sequence-Set_Testing2_Mutation_Negatives.fasta", "TestingSets/LTR_Sequence-Set_Mutation_Negative_Testing.fasta", "TestingSets/LTR_Sequence-Set_Mutation_Negative_Testing2.fasta", "TestingSets/LTR_first-50-bp_Sequence-Set_Testing_Mutation_Negatives.fasta", "TestingSets/LTR_first-50-bp_Sequence-Set_Testing2_Mutation_Negatives.fasta", "TestingSets/LTR_first-50-bp_Sequence-Set_Mutation_Negative_Testing.fasta", "TestingSets/LTR_first-50-bp_Sequence-Set_Mutation_Negative_Testing2.fasta", "TestingSets/LTR_first-100-bp_Sequence-Set_Testing_Mutation_Negatives.fasta", "TestingSets/LTR_first-100-bp_Sequence-Set_Testing2_Mutation_Negatives.fasta", "TestingSets/LTR_first-100-bp_Sequence-Set_Mutation_Negative_Testing.fasta", "TestingSets/LTR_first-100-bp_Sequence-Set_Mutation_Negative_Testing2.fasta", "TestingSets/LTR-U3_Sequence-Set_Repeats-Divided_Testing_Mutation_Negatives.fasta", "TestingSets/LTR-U3_Sequence-Set_Repeats-Divided_Testing2_Mutation_Negatives.fasta", "TestingSets/LTR-U3_Sequence-Set_Repeats-Divided_Mutation_Negative_Testing.fasta", "TestingSets/LTR-U3_Sequence-Set_Repeats-Divided_Mutation_Negative_Testing2.fasta", "TestingSets/LTR_last-50-bp_Sequence-Set_Testing_Mutation_Negatives.fasta", "TestingSets/LTR_last-50-bp_Sequence-Set_Testing2_Mutation_Negatives.fasta", "TestingSets/LTR_last-50-bp_Sequence-Set_Mutation_Negative_Testing.fasta", "TestingSets/LTR_last-50-bp_Sequence-Set_Mutation_Negative_Testing2.fasta", "TestingSets/LTR_last-100-bp_Sequence-Set_Testing_Mutation_Negatives.fasta", "TestingSets/LTR_last-100-bp_Sequence-Set_Testing2_Mutation_Negatives.fasta", "TestingSets/LTR_last-100-bp_Sequence-Set_Mutation_Negative_Testing.fasta", "TestingSets/LTR_last-100-bp_Sequence-Set_Mutation_Negative_Testing2.fasta", "TestingSets/LTR-U5_Sequence-Set_Repeats-Divided_Testing_Mutation_Negatives.fasta", "TestingSets/LTR-U5_Sequence-Set_Repeats-Divided_Testing2_Mutation_Negatives.fasta", "TestingSets/LTR-U5_Sequence-Set_Repeats-Divided_Mutation_Negative_Testing.fasta", "TestingSets/LTR-U5_Sequence-Set_Repeats-Divided_Mutation_Negative_Testing2.fasta"]
	
	testingReads = []
	for set in testingSets:
		testingReads.append(parseReadsFile(set))
	
	jsons = ["JSONs/LTR_Sequence-Set_Basic_Training1.json", "JSONs/LTR_Sequence-Set_Basic_Training2.json", "JSONs/LTR_Sequence-Set_Mutation_Training1.json", "JSONs/LTR_Sequence-Set_Mutation_Training2.json", "JSONs/LTR_first-50-bp_Sequence-Set_Basic_Training1.json", "JSONs/LTR_first-50-bp_Sequence-Set_Basic_Training2.json", "JSONs/LTR_first-50-bp_Sequence-Set_Mutation_Training1.json", "JSONs/LTR_first-50-bp_Sequence-Set_Mutation_Training2.json", "JSONs/LTR_first-100-bp_Sequence-Set_Basic_Training1.json", "JSONs/LTR_first-100-bp_Sequence-Set_Basic_Training2.json", "JSONs/LTR_first-100-bp_Sequence-Set_Mutation_Training1.json", "JSONs/LTR_first-100-bp_Sequence-Set_Mutation_Training2.json", "JSONs/LTR-U3_Sequence-Set_Basic_Training1.json", "JSONs/LTR-U3_Sequence-Set_Basic_Training2.json", "JSONs/LTR-U3_Sequence-Set_Mutation_Training1.json", "JSONs/LTR-U3_Sequence-Set_Mutation_Training2.json", "JSONs/LTR_last-50-bp_Sequence-Set_Basic_Training1.json", "JSONs/LTR_last-50-bp_Sequence-Set_Basic_Training2.json", "JSONs/LTR_last-50-bp_Sequence-Set_Mutation_Training1.json", "JSONs/LTR_last-50-bp_Sequence-Set_Mutation_Training2.json", "JSONs/LTR_last-100-bp_Sequence-Set_Basic_Training2.json", "JSONs/LTR_last-100-bp_Sequence-Set_Basic_Trainin.json", "JSONs/LTR_last-100-bp_Sequence-Set_Mutation_Training1.json", "JSONs/LTR_last-100-bp_Sequence-Set_Mutation_Training2.json", "JSONs/LTR-U5_Sequence-Set_Basic_Training1.json", "JSONs/LTR-U5_Sequence-Set_Basic_Training2.json", "JSONs/LTR-U5_Sequence-Set_Mutation_Training1.json", "JSONs/LTR-U5_Sequence-Set_Mutation_Training2.json"]
	
	models = []
	for m in jsons:
		models.append(loadHMMProfile(m))

	processes = mp.Pool(processes = 16)
	results = [processes.apply_async(applyHMMtoReads, args=(r, 0, 0, m, -100000000000)) for r, m in zip(testingReads, models)]
	VectorList = [p.get() for p in results]
	
	return VectorList

if __name__ == "__main__":
	import argparse
	
	parser = argparse.ArgumentParser()
	parser.add_argument("i", help="Input fasta file containing the reads to trim.", action="store", type=str)
	parser.add_argument("o", help="Output fasta file name containing trimmed reads.", action="store", type=str)
	parser.add_argument("l", help="Input txt file containing the output of LastZ alignment in tabular format.", action="store", type=str)
	parser.add_argument("p5", help="pol 5' HMM file in json format.", action="store", type=str)
	parser.add_argument("p5k", help="pol 5' HMM k-mer size to check.", action="store", type=int)
	parser.add_argument("p5s", help="pol 5' HMM stepping size to apply.", action="store", type=int)
	parser.add_argument("p5t", help="pol 5' HMM threshold to apply.", action="store", type=float)
	parser.add_argument("p3", help="pol 3' HMM file in json format.", action="store", type=str)
	parser.add_argument("p3k", help="pol 3' HMM k-mer size to check.", action="store", type=int)
	parser.add_argument("p3s", help="pol 3' HMM stepping size to apply.", action="store", type=int)
	parser.add_argument("p3t", help="pol 3' HMM threshold to apply.", action="store", type=float)
	parser.add_argument("u3", help="U3 LTR HMM file in json format.", action="store", type=str)
	parser.add_argument("u3k", help="U3 LTR HMM k-mer size to check.", action="store", type=int)
	parser.add_argument("u3s", help="U3 LTR HMM stepping size to apply.", action="store", type=int)
	parser.add_argument("u3t", help="U3 LTR HMM threshold to apply.", action="store", type=float)
	parser.add_argument("u5", help="U5 LTR HMM file in json format.", action="store", type=str)
	parser.add_argument("u5k", help="U5 LTR HMM k-mer size to check.", action="store", type=int)
	parser.add_argument("u5s", help="U5 LTR HMM stepping size to apply.", action="store", type=int)
	parser.add_argument("u5t", help="U5 LTR HMM threshold to apply.", action="store", type=float)
	parser.add_argument("-process", dest="proc", help="maximum number of processes to run in parallel", action="store", type=int)
	
	parser.add_argument("-five", help="Name for file saving sequences from 5'.", action="store", type=str)
	parser.add_argument("-three", help="Name for file saving sequences from 3'.", action="store", type=str)
	parser.add_argument("-comp", help="Name for file saving complete sequences.", action="store", type=str)
	parser.add_argument("-trun5", help="Name for file saving sequences truncated at 5' side.", action="store", type=str)
	parser.add_argument("-trun3", help="Name for file saving sequences truncated at 3' side.", action="store", type=str)
	
	args = parser.parse_args()
	
	import multiprocessing as mp
	
	reads = parseReadsFile(args.i)
	
	jsons = [args.p5, args.p3, args.u3, args.u5]
	
	models = []
	for j in jsons:
		models.append(loadHMMProfile(j))
	
	ks = [args.p5k, args.p3k, args.u3k, args.u5k]
	ss = [args.p5s, args.p3s, args.u3s, args.u5s]
	ts = [args.p5t, args.p3t, args.u3t, args.u5t]
	
	if args.proc is None:
		processes = mp.Pool(processes = 4)
	else:
		processes = mp.Pool(processes = args.proc)
	results = [processes.apply_async(applyHMMtoReads, args=(reads, k, s, m, t)) for k, s, m, t in zip(ks, ss, models, ts)]
	positions = [p.get() for p in results]
	
	LastZ_positions = retrieveLastZpositions(args.l, reads)
	
	trimmed = trimSequences(reads, positions[0][0], positions[1][0], positions[2][0], positions[3][0], LastZ_positions, args.five, args.three, args.comp, args.trun5, args.trun3)
	
	writeTrimmed(trimmed, args.o)
