# T-CASE: Targeted CAS Sequencing Enrichment - Analyzer

T-CASE is a library preparation methodology for the study of un-localized fragments and the detection and study of their surroundings in the genome. 
Analysis pipelines have been implemented with Nextflow to analyze and simulate T-CASE data.

* T-CASE:Simulator: This pipeline allows the simulation of T-CASE library preparation and sequencing experimental process from an initial data set of sequences.
* T-CASE:Analyzer: This pipeline allows the automated analysis of the sequencing data from T-CASE library preparation.

To use [T-CASE:Simulator](https://bitbucket.org/synbiolab/t-case-simulator/src) please refer to Bitbucket repository linked.

## Getting Started

### Prerequisites

To be able to use T-CASE pipeline you will need to install [Nextflow](https://www.nextflow.io/), please follow [Installation instructions](https://www.nextflow.io/docs/latest/getstarted.html#installation).

A [Docker](https://www.docker.com/) container is proveded in order to run T-CASE avoiding the need of installing programes and libraries on your PC, in order to use it, please [install Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/).

## Running the example

In order to run the program with the provided example data pull the Docker image to your local machine:

```bash
docker pull synbio/t-case:analyzer
```

And use use the online Bitbucket repository [1] or clone the repository to your local machine [2]:

[1]
```bash
nextflow pull https://bitbucket.org/synbiolab/t-case-analyzer
nextflow run https://bitbucket.org/synbiolab/t-case-analyzer
```
[2]
```bash
git clone https://Julia_MP@bitbucket.org/synbiolab/t-case-analyzer.git 
cd t-case-analyzer
nextflow run main.nf
```

## Deployment

In order to run the program with your own data you have two options:

* Modify nextflow.config file and run normally
* Create a new nextflow2.config file adding your own data and run with the following command.

```bash
nextflow -C nextflow2.config run https://bitbucket.org/synbiolab/t-case-analyzer # to substitute provided nextflow.config file
nextflow -c nextflow2.config run https://bitbucket.org/synbiolab/t-case-analyzer # to merge both config files
```

If you wish to run the pipeline with Singularity instead of Docker, please pull the Docker image and create a Singularity one.

```bash
singularity pull 't-case_analyzer.sif' docker://synbio/t-case:analyzer
```

Create a nextflow_singularity.config file including the following information.

```groovy
docker.enabled = false
singularity.enabled = true
singularity.autoMounts = true
process.container = 't-case_analyzer.sif'
```

And run normally merging both configuration files.

```bash
nextflow -c nextflow_singularity.config run https://bitbucket.org/synbiolab/t-case-analyzer
```

## Output

Nextflow generates two different output directories:

#### Output
Contains mapping output files in sorted.bam and sorted.bam.bai format.
Conatins mapping output files filtered by prmiary alignments only in sorted.bam and sorted.bam.bai format.
Contains annotation counts file.
Contains chromosome view plot.
Contains reconstituted annotations in bed format.
Contains files required to visualize the Genome Browser. **GenomeBrowser** folder contains two folders: **examples**, with html and js file. **test-data**, with bigBed annotations, alignments in bam and bam.bai format and reference genome in 2bit format.
#### ExtraOutput
Contains LastZ alignment output in tabular format.
Contains filtered input fasta file with only reads that present a hit.
Contains trimmed input fasta file reads.

### Genome Browser visualization

*Not Implemented*

In order to visualize the generated Genome Browser, you will need to use the provided Docker image and output files inside GenomeBrowser folder.

First of all, enter inside your Docker container redirecting ports:

```bash
docker run -it -p 8080:80 synbio/t-case:analyzer bash
```

In another terminal, look for the running container ID with ps command (it should look like 847a86224980) and copy the files inside the Docker container:

```bash
docker ps
docker cp Output/GenomeBrowser/examples/. 847a86224980:/workdir/pileup.js/examples/
docker cp Output/GenomeBrowser/test-data/. 847a86224980:/workdir/pileup.js/test-data/
```

Finally, inside your Docker container run an http-server with npm:

```bash
cd /workdir/pileup.js
npm run http-server
```

You can open the Genome Browser in your Browser of preference by using the name of the generated .html file [http://172.17.0.2:8080/examples/YourFileName.html](http://172.17.0.2:8080/examples/YourFileName.html)

### Configuration file parameters

* input: Path to the fasta file containing the initial read sequences.
* processes: Number of processes to run simultaniously when parallelizing
* path: Path where nextflow project is stored, default is .nextflow folder at $HOME, or custom folder where repository has been pulled.
* refTarget: Reference sequence in fasta format used to select reads that contain a hit. 
* LastZidentity: Percentage identity threshold to select a hit alignment.
* LastZlength: Percentage length threshold to select a hit alignment.
* pol5, pol3, U3, U5: JSON files containing HMM to find 5' pol gene, 3' pol gene U3 LTR and U5 LTR respectively. Used when trimming retroviruse targeted sequences.
* pol5k, pol3k, U3k, U5k: K-mer size to analyze HMMs for 5' pol gene, 3' pol gene, U3 TR and U5 LTR respectively.
* pol5s, pol3s, U3s, U5s: Stepping size to analyze HMMs for 5' pol gene, 3' pol gene, U3 TR and U5 LTR respectively.
* pol5t, pol3t, U3t, U5t: Threshold used to select analyzed HMMs for  5' pol gene, 3' pol gene, U3 TR and U5 LTR respectively.
* refGenome: Path to reference genome file.
* refGff: Path to reference annotations file in gff3 format.
* dist: Distance at which two reads are considered a pair.
* chromSize: Path to file containing chromosome sizes.
* refGen2bin: Path to reference genome in 2bit format.
* refAnnBd: Path to reference annotations in bigBed format.

## Authors

* **Júlia Mir Pedrol** - *Initial work*
