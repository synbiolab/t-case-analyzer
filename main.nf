#!/usr/bin/env nextflow

inputFiles = Channel.fromPath( params.input )

process findHits {
	publishDir "ExtraOutput", mode: 'copy', pattern: '*.txt'

	input:
	file(reads) from inputFiles
	file(ref) from file(params.refTarget)

	output:
	file("${reads.baseName}.txt") into Alignment
	file("$reads") into inputReads
	file("$reads") into inputNames

	script:
	"""
	lastz_32 $reads[multiple] $ref --ambiguous=iupac --format=general --output=${reads.baseName}.txt
	"""
}

process parseAlignment {
	input:
	file(alignment) from Alignment
	val id from params.LastZidentity
	val len from params.LastZlength

	output:
	file("${alignment.baseName}_toSelect.txt") into Select
	file("${alignment.baseName}_toTrim.txt") into Trim

	"""
	parseLastZ.py -id $id -len $len $alignment ${alignment.baseName}_toSelect.txt ${alignment.baseName}_toTrim.txt
	"""
}

process filterHits {
	publishDir "ExtraOutput", mode: 'copy'

	input:
	file(reads) from inputReads
	file(sel) from Select

	output:
	file("${reads.baseName}_Filtered.fasta") into Filtered

	"""
	filterbyname.sh in=$reads out=${reads.baseName}_Filtered.fasta names=$sel include=t substring=name
	"""
}

process trimReads {
	publishDir "ExtraOutput", mode: 'copy', pattern: '*.fasta'

	input:
	file(reads) from Filtered
	file(trim) from Trim
	file(pol5) from file(params.pol5)
	val pol5k from params.pol5k
	val pol5s from params.pol5s
	val pol5t from params.pol5t
	file(pol3) from file(params.pol3)
	val pol3k from params.pol3k
	val pol3s from params.pol3s
	val pol3t from params.pol3t
	file(U3) from file(params.U3)
	val U3k from params.U3k
	val U3s from params.U3s
	val U3t from params.U3t
	file(U5) from file(params.U5)
	val U5k from params.U5k
	val U5s from params.U5s
	val U5t from params.U5t
	val proc from params.processes

	output:
	file("${reads.baseName}_trimmed.fasta") into Trimmed
	file("${reads.baseName}_at5p.txt") into P5
	file("${reads.baseName}_at3p.txt") into P3
	file("${reads.baseName}_complete.txt") into Complete
	file("${reads.baseName}_5pTrunc.txt") into Trunc5
	file("${reads.baseName}_3pTrunc.txt") into Trunc3

	"""
	trimReads_HMM.py $reads ${reads.baseName}_trimmed.fasta $trim $pol5 $pol5k $pol5s $pol5t $pol3 $pol3k $pol3s $pol3t $U3 $U3k $U3s $U3t $U5 $U5k $U5s $U5t -process $proc -five ${reads.baseName}_at5p.txt -three ${reads.baseName}_at3p.txt -comp ${reads.baseName}_complete.txt -trun5 ${reads.baseName}_5pTrunc.txt -trun3 ${reads.baseName}_3pTrunc.txt
	"""
}

process mapping {
	input:
	file(ref) from file(params.refGenome)
	file(reads) from Trimmed

	output:
	file("${reads.baseName}_mapped.sam") into Mapped_Raw
	file("${reads.baseName}_mapped.sam") into Mapped_Primary

	"""
	minimap2 -ax map-ont $ref $reads > ${reads.baseName}_mapped.sam
	"""
}

process formatMapRaw {
	publishDir "Output", mode: 'copy'

	input:
	file(mapped) from Mapped_Raw

	output:
	file("${mapped.baseName}.sorted.bam") into RawSorted
	file("${mapped.baseName}.sorted.bam.bai") into RawBai

	"""
	samtools view -Sb -F 4 $mapped > ${mapped.baseName}.bam
	samtools sort -o ${mapped.baseName}.sorted.bam ${mapped.baseName}.bam
	samtools index -b ${mapped.baseName}.sorted.bam > ${mapped.baseName}.sorted.bam.bai
	"""
}

process formatMapPrimary {
	publishDir "Output", mode: 'copy'

	input:
	file(mapped) from Mapped_Primary

	output:
	file("${mapped.baseName}_primary.sorted.bam") into PrimarySorted
	file("${mapped.baseName}_primary.sorted.bam") into PrimaryBam
	file("${mapped.baseName}_primary.sorted.bam") into GBBam
	file("${mapped.baseName}_primary.sorted.bam.bai") into PrimaryBai

	"""
	samtools view -Sb -F 4 -F 256 -F 2048 $mapped > ${mapped.baseName}_primary.bam
	samtools sort -o ${mapped.baseName}_primary.sorted.bam ${mapped.baseName}_primary.bam
	samtools index -b ${mapped.baseName}_primary.sorted.bam > ${mapped.baseName}_primary.sorted.bam.bai
	"""
}

process countAnnotations_Primary {
	publishDir "Output", mode: 'copy'	

	input:
	file(mapped) from PrimarySorted
	file(ref) from file(params.refGff)

	output:
	file("${mapped.baseName}-alignments_annotations.count") into Annotations

	"""
	countAnnotations.R $mapped $ref ${mapped.baseName}-alignments_annotations.count
	"""
}


process formatOutput {
	publishDir "Output", mode: 'copy'
	
	input:
	file(bam) from PrimaryBam
	file(trunc5) from Trunc5
	file(trunc3) from Trunc3
	file(at5p) from P5
	file(at3p) from P3
	val dist from params.dist
	val path from params.path

	output:
	file("${bam.baseName}_chromView.pdf") into Karyo
	file("${bam.baseName}_annotations.bed") into Bed
	
	
	"""
	outputFormat.R $bam ${bam.baseName}_chromView.pdf $trunc5 $trunc3 $at5p $at3p $dist ${bam.baseName}_annotations.bed ${path}/bin/detectTruncatedPERV.py
	"""
}

process genomeBrowser {
	publishDir "Output/GenomeBrowser/examples", mode: 'copy', pattern: '*.{html,js}'
	publishDir "Output/GenomeBrowser/test-data", mode: 'copy', pattern: '*.{bb,bam,bai,2bit}'

	input:
	file(name) from inputNames
	file(bed) from Bed
	file(chrom) from file(params.chromSize)
	file(readsbam) from GBBam
	file(readsbai) from PrimaryBai
	file(bit) from file(params.refGen2bit)
	file(annot) from file(params.refAnnBb)

	output:
	file("${name.baseName}.html") into Html
	file("${name.baseName}.js") into Js
	file("${bed.baseName}.bb") into BB
	file("$readsbam") into ReadsBam
	file("$readsbai") into ReadsBai
	file("$bit") into BitRef
	
	"""
	# Create html file
	
	echo -e '<!doctype html>\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n<link rel="stylesheet" href="../style/pileup.css" />\n<link rel="stylesheet" href="demo.css" />\n</head>\n\n<body>\n<button id="jiggle">FPS test</button>\n<div id="pileup"></div>\n</body>\n\n<script src="../node_modules/stats.js/build/stats.min.js"></script>\n<script src="../dist/pileup.js"></script>\n\n<script src="${name.baseName}.js"></script>\n<script src="playground.js"></script>' > ${name.baseName}.html
	
	# Convert bed to bigBed format
		
	sort -k1,1 -k2,2n $bed > ${bed.baseName}.sorted.bed
	/workdir/bedToBigBed -type=bed9 ${bed.baseName}.sorted.bed $chrom ${bed.baseName}.bb

	# Create js file

	echo -e 'var bamSource = pileup.formats.bam({\n\turl: "/test-data/$readsbam",\n\tindexUrl: "/test-data/$readsbai"\n});' > ${name.baseName}.js
	echo -e 'var sources = [\n\t{\n\t\tviz: pileup.viz.genome(),\n\t\tisReference: true,\n\t\tdata: pileup.formats.twoBit({\n\t\t\turl: "$bit"\n\t\t}),\n\t\tname: "Reference"\n\t},' >> ${name.baseName}.js
 	echo -e '\t{\n\t\tviz: pileup.viz.scale(),\n\t\tname: "Scale"\n\t},\n\t{\n\t\tviz: pileup.viz.location(),\n\t\tname: "Location"\n\t},\n\t{\n\t\tviz: pileup.viz.coverage(),\n\t\tdata: bamSource,\n\t\tname: "Coverage"\n\t},\n\t{\n\t\tviz: pileup.viz.pileup(),\n\t\tdata: bamSource,\n\t\tname: "Reads"\n\t},' >> ${name.baseName}.js
	echo -e '\t{\n\t\tviz: pileup.viz.genes(),\n\t\tdata: pileup.formats.bigBed({\n\t\t\turl: "/test-data/${bed.baseName}.bb"\n\t\t}),\n\t\tname: "PERVs"\n\t},\n\t{\n\t\tviz: pileup.viz.genes(),\n\t\tdata: pileup.formats.bigBed({\n\t\t\turl: "/test-data/$annot"\n\t\t}),\n\t\tname: "Genes"\n\t}\n];' >> ${name.baseName}.js
	"""
}

